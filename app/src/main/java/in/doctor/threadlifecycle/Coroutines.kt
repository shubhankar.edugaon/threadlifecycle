package doctor.threadlifecycle


import kotlinx.coroutines.*

suspend fun main(){

    runBlocking {
        println("Main program start: ${Thread.currentThread().name}")

        GlobalScope.launch {
            println("Fake work starts: ${Thread.currentThread().name}")
            delay(1000)
            println("Fake work finished: ${Thread.currentThread().name}")
        }

        println("Main program ends: ${Thread.currentThread().name}")
    }
}

